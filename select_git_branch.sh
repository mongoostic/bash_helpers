#!/bin/bash

# DESCRIPTION
# Данный скрипт предназначен для того, чтобы упростить частое переключение между ветками git в проектах
# чаще всего, когда нужно переключаться с одной задачи на другую
# 
# Скрипт показывает 5 последних веток, отсортированных по датам коммитов
#
# основной кейс
# данный скрипт лежит по пути 
#       ~/myselfsettings/bash_helpers/select_git_branch.sh
# в .bashrc (или в .mybashrc, т.к. у меня алиасы подключаются через отдельный скрипт) прописываем alias
#       alias b=~/myselfsettings/bash_helpers/select_git_branch.sh

# Для вывода select в один столбец, нужно ограничить COLUMNS
COLUMNS=20

options=( $(git for-each-ref --format='%(refname:short)' --sort=committerdate refs/heads/ | tail -8) quit )

select opt in "${options[@]}";
do

if [ $opt == 'quit' ] ;
then
	break;
else 
	git checkout $opt;
	break;
fi

done
