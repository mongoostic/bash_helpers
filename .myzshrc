# Zsh additional configuration file
# Author: Anton Tiutiunnikov
#
# Полезные утилиты
# z - jump around

# традиционный стиль перенаправлений fd
unsetopt MULTIOS
# поддержка ~… и file completion после = в аргументах
setopt MAGIC_EQUAL_SUBST
# не обрабатывать escape sequence в echo без -e
setopt BSD_ECHO
# поддержка комментариев в командной строке
setopt INTERACTIVE_COMMENTS
# поддержка $(cmd) в $PS1 etc.
setopt PROMPT_SUBST

#################### TMUX settings ########################
alias tmux="TERM=tmux-256color tmux"

if [[ $SHLVL = "1" ]]
then
    #tmux -S /var/tmux/$USER new-session -A -s "system"
    # this need, as at start tmux, again run bash and we accept message
    # sessions should be nested with care, unset $TMUX to force
    tmux new-session -d -s "python"
    tmux new-session -d -s "payout"
    tmux new-session -d -s "tricks"
    tmux new-session -d -s "self"
    tmux new-session -d -s "clm"
    tmux new-session -A -s "dmr"
fi

function to {
    tmux switch -t "$1"
}

# for tmux alerting
precmd () {
  echo -n -e "\a"
}

#################### PATH settings ######################
export PATH="$HOME/bin:$PATH"

#################### Colors and Prompt ##################
# Pure plugin settings (https://github.com/sindresorhus/pure)
zmodload zsh/nearcolor
autoload -U colors && colors

# required for pure plugin
autoload -U promptinit; promptinit

# change the color for both `prompt:success` and `prompt:error`
zstyle ':prompt:pure:prompt:*' color cyan
zstyle :prompt:pure:git:branch color yellow
zstyle :prompt:pure:git:dirty color red

zstyle :prompt:pure:git:stash show yes
zstyle :prompt:pure:git:stash color cyan

zstyle :prompt:pure:git:arrow show yes
zstyle :prompt:pure:git:arrow color cyan

zstyle :prompt:pure:prompt:continuation color cyan

source /usr/local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
prompt pure
################################################################
############## Git autocompletion support ######################
autoload -Uz compinit && compinit
################################################################

############## Locale settings #################################
export LC_ALL="ru_RU.UTF-8"
export LANG="ru_RU.UTF-8"
################################################################
#
export RIPGREP_CONFIG_PATH=~/.ripgreprc

# Утилита для переключения каталогов
. ~/.mysettings/bash_helpers/z.sh

# Python pyenv support
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

# Python3 utils
pyclean () {
    find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete
}

# Bash commands helper (https://unix.stackexchange.com/a/18092)
bashman () { man bash | less -p "^       $1 "; }

# Вспомогательные команды
convert_cp1251_to_utf8 () {
	TEMPFILE=$(mktemp /tmp/tmpfile.XXXXXX)
	iconv -f cp1251 -t utf8 -s $1 > $TEMPFILE
	mv $TEMPFILE $1
}

convert_utf8_to_cp1251 () {
	TEMPFILE=$(mktemp /tmp/tmpfile.XXXXXX)
	iconv -f utf8 -t cp1251 -s $1 > $TEMPFILE
	mv $TEMPFILE $1
}

# Применить алисы
source ~/.mysettings/bash_helpers/.myzsh_aliases
